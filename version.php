<?php
$plugin->component = 'block_cybernetica_weather';  // Plugin name
$plugin->version = 2021041000;  // Current version
$plugin->requires = 20210325; // YYYYMMDDHH Required Moodle version (Block was tested only on this version)
$plugin->release = 'v1.0';