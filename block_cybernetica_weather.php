<?php

/*
 * Constant if in the future we want to change the name of the plugin.
 */
const PLUGIN_NAME = "block_cybernetica_weather";

class block_cybernetica_weather extends block_base
{
    public $api;
    public $user;

    /*
     * Initialize class values.
     */
    public function init()
    {
        global $USER;

        $this->title = get_string('pluginname', PLUGIN_NAME);

        $this->api = get_config(PLUGIN_NAME, 'openweatherapi');
        $this->user = $USER;
    }

    /*
     * Enabling config in settings.php
     *
     * @return boolean
     */
    function has_config()
    {
        return true;
    }

    /*
     * Shortens the call of get_string().
     *
     * @return get_string text
     */
    public function get_string($key)
    {
        return get_string($key, PLUGIN_NAME);
    }

    /*
     * Function that makes request to OpenWeather API.
     * Checking valid xml and valid city.
     * Returning xml if ok, otherwise returning null.
     *
     * @return xml or null
     */
    public function get_city_xml()
    {
        $city = preg_replace('/\s+/', '+', $this->user->city); //Replacing spaces in cities names with '+' for right parsing

        $url = 'http://api.openweathermap.org/data/2.5/weather?q=' . $city . '&appid=' . $this->api . '&units=metric&mode=xml';

        if (!$xml = simplexml_load_file($url)) {
            $this->content->text = $this->get_string('falseapi');
            return null;
        } else if ($xml->cod == '404') {
            $this->content->text = $this->get_string('falsecity');
            return null;
        } else {
            return $xml;
        }
    }

    /*
     * Parses correct xml to retrieve data to show.
     *
     * @return array['temperature', 'feels_like', 'icon', 'city']
     */
    public function parse_city_xml($xml)
    {
        $temperature = $xml->temperature['value'];
        $feels_like = $xml->feels_like['value'];
        $icon = $xml->weather['icon'];
        $city = $xml->city['name'];
        return compact('temperature', 'feels_like', 'icon', 'city');
    }

    /*
     * Our block content.
     *
     * @return stdClass
     */
    public function get_content()
    {
        if ($this->content !== null) {
            return $this->content;
        }

        $this->content = new stdClass;

        if (empty($this->api)) {
            $this->content->text = $this->get_string('noapi');
            return $this->content;
        } else if (empty($this->user->city)) {
            $this->content->text = $this->get_string('nocity');
            return $this->content;
        } else if (!$xml = $this->get_city_xml()) {
            return null;
        } else {
            $this->content->text = $this->get_content_text($this->parse_city_xml($xml));
        }

        return $this->content;
    }

    /*
     * Template for our block.
     *
     * @return string
     */
    public function get_content_text($data)
    {
        $plugin_template = '<h1>' . $data['city'] . '</h1> <br>';
        $plugin_template .= '<img src="http://openweathermap.org/img/wn/' . $data['icon'] . '@2x.png" align="left" width="50px" height="50px">';
        $plugin_template .= '<h1>' . round($data['temperature']) . '°C </h1>';
        $plugin_template .= '<b>' . get_string('feelslike', 'block_cybernetica_weather') . ' ' . round($data['feels_like']) . '°C </b>';

        return $plugin_template;
    }

    /*
     * Testing for correct installation or successful update.
     *
     * @return boolean
     */
    function _self_test()
    {
        return true;
    }
}