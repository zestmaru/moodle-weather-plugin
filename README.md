## Moodle weather block
This is a Moodle plugin which shows the weather in a location defined by the city in Moodle user profile. <br>
This plugin uses only <a href="https://openweathermap.org/api">OpenWeather API </a>. <br><br>
![Block image preview](./block.png)