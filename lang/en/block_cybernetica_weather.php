<?php
$string['pluginname'] = 'Weather';
$string['nocity'] = 'Please add city in your profile before using this plugin.';
$string['falsecity'] = 'Please check your city spelling in your profile.';
$string['feelslike'] = 'Feels like';
$string['simplehtml:addinstance'] = 'Add a new simple Weather block.';
$string['simplehtml:myaddinstance'] = 'Add a new simple Weather block to the My Moodle page.';
$string['installtitle'] = 'Please provide your OpenWeather API key.';
$string['installdesc'] = 'OpenWeather API key here.';
$string['installdefault'] = 'This field not should be empty!';
$string['noapi'] = 'Please contact with your admin to provide API key.';
$string['falseapi'] = 'Please contact with your admin to provide a valid API key.';