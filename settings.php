<?php

global $ADMIN;

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {
    $name = 'block_cybernetica_weather/openweatherapi';
    $title = get_string('installtitle', 'block_cybernetica_weather');
    $description = get_string('installdesc', 'block_cybernetica_weather');
    $default = get_string('installdefault', 'block_cybernetica_weather');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $settings->add($setting);
}